# Project 2 #

### What is this repository for? ###

This is a small test balloon to see how git submodules are working. This is one of the two projects which will include [Shared](../../../gsub-shared/)

### How do I get set up? ###

Check out all repositories of this Bitbucket project.

### Who do I talk to? ###

There is no support on this project

### License ###

This project is published under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).